package com.pixeltrice.springbootimportcsvfileapp;

import org.apache.commons.csv.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;


public class CSVHelper {
    public static String TYPE = "text/csv";

    //  static String[] HEADERs = { "Id", "Title", "Description", "Published" };
//
    public static boolean hasCSVFormat(MultipartFile file) {
        System.out.println(file.getContentType());
        if (TYPE.equals(file.getContentType())
                || file.getContentType().equals("application/vnd.")) {
            return true;
        }

        return false;
    }

    //ghji da ta
    public static List<DeveloperTutorial> csvToTutorials(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

            List<DeveloperTutorial> developerTutorialList = new ArrayList<>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                DeveloperTutorial developerTutorial = new DeveloperTutorial(
                        Long.parseLong(csvRecord.get("Id")),
                        csvRecord.get("name"),
                        csvRecord.get("diemso")

                );

                developerTutorialList.add(developerTutorial);
            }

            return developerTutorialList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }

    //
//    public static List<DeveloperTutorial> tutorialsToCSV(List<DeveloperTutorial> developerTutorialList) {
//        final CSVFormat format = CSVFormat.DEFAULT.withQuoteMode(QuoteMode.MINIMAL);
//
//        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
//             CSVPrinter csvPrinter = new CSVPrinter(new PrintWriter(out), format);) {
//            for (DeveloperTutorial developerTutorial : developerTutorialList) {
//                List<String> data = Arrays.asList(
//                        String.valueOf(developerTutorial.getId()),
//                        developerTutorial.getTitle(),
//                        developerTutorial.getDescription(),
//                        String.valueOf(developerTutorial.isPublished())
//                );
//
//                csvPrinter.printRecord(data);
//            }
//
//            csvPrinter.flush();
//            return (List<DeveloperTutorial>) new ByteArrayInputStream(out.toByteArray());
//        } catch (IOException e) {
//            throw new RuntimeException("fail to import data to CSV file: " + e.getMessage());
//        }
//    }

    public static List<DeveloperTutorial> readData(MultipartFile file) throws IOException {
        String line = null;
        List<DeveloperTutorial> tutorials = new ArrayList<>();
//        try {
        BufferedReader input = new BufferedReader(new InputStreamReader(file.getInputStream(), "UTF-8"));
        Map<String, List<Long>> result = new HashMap<String, List<Long>>();


        while ((line = input.readLine()) != null && !line.isEmpty()) {
            String[] fields = parseCsvLine(line);


            String key = fields[0].trim();
            Long  value;
            if (fields.length == 1) {
                value = 0l;
            } else {
                value = Long.valueOf(fields[fields.length - 1].trim().replace("[|]", ""));
            }
            if(value < 0){
                value = 0l;
            }

            if (result.get(key) == null) {

                List<Long> lis = new ArrayList<>();
                lis.add(value);
                result.put(key, lis);
            } else {

                List<Long> lis = result.get(key);
                lis.add(value);
                result.put(key, lis);
            }
        }
        if (result != null && !result.isEmpty() && !result.entrySet().isEmpty()) {
            for (Map.Entry r : result.entrySet()) {
                System.out.println(r.getKey() + ": " + r.getValue());
                List<Long> lsInt = (List<Long>) r.getValue();
                Long sum = lsInt.stream()
                        .reduce(0L, Long::sum);
                DeveloperTutorial tutorial = new DeveloperTutorial();
                tutorial.setId(null);
                tutorial.setTen("Sum_" + r.getKey().toString());
                tutorial.setDiemso(Long.valueOf(sum));
                tutorials.add(tutorial);
            }
        }
        return tutorials;
    }


    private static String[] parseCsvLine(String line) {
        Pattern p = Pattern.compile(",(?=([^\"]*\"[^\"]*\")*(?![^\"]*\"))");
        // Split input with the pattern
        String[] fields = p.split(line);
        String regex = ",|\"";
        for (int i = 0; i < fields.length; i++) {
            // Get rid of residual double quotes
            fields[i] = fields[i].replaceAll(regex, "");
        }
        return fields;
    }

    //khawuy
}
