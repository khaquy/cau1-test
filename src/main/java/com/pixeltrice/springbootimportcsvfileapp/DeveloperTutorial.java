package com.pixeltrice.springbootimportcsvfileapp;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "developer_tutorial")
public class DeveloperTutorial {

	  @Id
	  @Column(name = "id")
	  @GeneratedValue(strategy=GenerationType.IDENTITY)
	  private Long id;

	  @Column(name = "ten")
	  private String ten;

	  @Column(name = "diemso")
	  private Long diemso;

	public DeveloperTutorial(Long id, String name, String diemso) {
	}

	public DeveloperTutorial() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "DeveloperTutorial{" +
				"id=" + id +
				", ten='" + ten + '\'' +
				", diemso=" + diemso +
				'}';
	}

	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}

	public Long getDiemso() {
		return diemso;
	}

	public void setDiemso(Long diemso) {
		this.diemso = diemso;
	}

	public DeveloperTutorial(Long id, String ten, Long diemso) {
		this.id = id;
		this.ten = ten;
		this.diemso = diemso;
	}

	public DeveloperTutorial(List<Integer> lis) {

	  }


//	  public Long getId() {
//	    return id;
//	  }
//
//	  public void setId(Long id) {
//	    this.id = id;
//	  }
//
//	  public String getTitle() {
//	    return title;
//	  }
//
//	  public void setTitle(String title) {
//	    this.title = title;
//	  }
//
//	  public String getDescription() {
//	    return description;
//	  }
//
//	  public void setDescription(String description) {
//	    this.description = description;
//	  }
//
//	  public boolean isPublished() {
//	    return published;
//	  }
//
//	  public void setPublished(boolean isPublished) {
//	    this.published = isPublished;
//	  }



}
